<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
if (file_exists(dirname(__FILE__) . '/local.php')) {
	// Local database settings
	define( 'DB_NAME', 'local' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', 'root' );
	define( 'DB_HOST', 'localhost' );
}else {
	// Live database settings
	define( 'DB_NAME', 'arturd73_universitydata' );
	define( 'DB_USER', 'arturd73_wp20' );
	define( 'DB_PASSWORD', 'Dbsport2022' );
	define( 'DB_HOST', 'localhost' );
}
/** The name of the database for WordPress */


/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z5G2UBKG8Tw7FPQwMjwWElUK749hOi7BNP3nbPtxn2jAugLW/UsnQyGYE88F5DcM3JIIo1Uo/lAyAvUPyAqRLg==');
define('SECURE_AUTH_KEY',  'ds88qOxPUpHJ9NQbuVYO6Uy/lLGmoPu0sY+8yNaninHcUouuVvitTpqF5HR+L7YbPj1fvfgeTcgyo/eI5xpkXA==');
define('LOGGED_IN_KEY',    '36RBlf/UZ0gEzbaadagV6gJaXvhb48XpoFKsQNAAMtyf+DQ6E7RvUwX5WaSEYvb7EBQkXAT92BhTBms9GbKDZQ==');
define('NONCE_KEY',        'hcGrXWrbHiJKfzlHvYboq9foaP3DUDwIKVS2WRUcG8T24WDTHunhdKrY47+kvbReuPAwXtO8S/sgTZHWu6ecUQ==');
define('AUTH_SALT',        'tusNhQ6uLmEDrFVmlxDXcRDCIIaZjmNqcR0bKuBX3Ir/NYtwsR1B7DZewnsL974s7zoPkK8j+Ko+9alFCinTXA==');
define('SECURE_AUTH_SALT', 'kA492YXQpvYj1Whr43Ec8769L4/pETg1upKP280+HaccfrSQcFvsVgBMmD9+En0kIBc9C9gig00j1ezy3GmvAQ==');
define('LOGGED_IN_SALT',   'fQm1NAFkUtlRzlxwYBsY2pvIocTeUKImAqR5fYOg5pQmj73v8Vw0kFRxc6wwEMuQ2O8/IBATliq9OCpHsR2tuA==');
define('NONCE_SALT',       'oyEjKObmK9jGf8S0UMSPFiB2IgxLbYaROHtzT1Gf1SOIoDDQa5QOSmKssCqcrEkXfHmNyABrLz3OZLQAv6iaQQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';





/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}

/* Inserted by Local by Flywheel. Fixes $is_nginx global for rewrites. */
if (strpos($_SERVER['SERVER_SOFTWARE'], 'Flywheel/') !== false) {
	$_SERVER['SERVER_SOFTWARE'] = 'nginx/1.10.1';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
